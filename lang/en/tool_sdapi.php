<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

$string['pluginname'] = 'Sitedirectory API';
$string['pluginname_desc'] = 'To connect to the Sitedirectory DB this plugin uses the DB connection parameters configured for the <a href="{$a}">DB enrol plugin</a>.';
$string['privacy:metadata'] = 'The Sitedirectory API only provides an interface to an external DB.';
$string['expire_invitations_task'] = 'Expire invitations';

$string['settingsheaderuser'] = 'User table';
$string['user_table'] = 'User table name';
$string['user_table_desc'] = 'Name of the user table in the SiteDirectory.';
$string['user_table_moodleid_field'] = 'Moodle-ID field';
$string['user_table_moodleid_field_desc'] = 'Name of Moodle-ID field in the user table.';
$string['user_table_lmsid_field'] = 'LMS-ID field';
$string['user_table_lmsid_field_desc'] = 'Name of LMS-ID field in the user table.';
$string['user_table_timid_field'] = 'TIM-ID field';
$string['user_table_timid_field_desc'] = 'Name of TIM-ID field in the user table.';
$string['user_table_matrnr_field'] = 'Immatriculation number field';
$string['user_table_matrnr_field_desc'] = 'Name of immatriculation number field in the user table.';
$string['user_table_gguid_field'] = 'GGUID field';
$string['user_table_gguid_field_desc'] = 'Name of GGUID field in the user table.';
$string['user_table_ronid_field'] = 'RON-ID field';
$string['user_table_ronid_field_desc'] = 'Name of RON-ID field in the user table.';
$string['user_table_deleted_field'] = '"Deleted" field';
$string['user_table_deleted_field_desc'] = 'Name of "Deleted" field in the user table. This field indicates whether a user should be considered deleted.';

$string['settingsheaderenrolment'] = 'Enrolment table';
$string['enrolment_table'] = 'Enrolment table name';
$string['enrolment_table_desc'] = 'Name of the enrolment table in the SiteDirectory.';
$string['enrolment_table_moodleid_field'] = 'Moodle-ID field';
$string['enrolment_table_moodleid_field_desc'] = 'Name of the Moodle-ID field in the enrolment table.';
$string['enrolment_table_course_idnumber_field'] = 'Course idnumber field';
$string['enrolment_table_course_idnumber_field_desc'] = 'Name of the course idnumber field in the enrolment table.';
$string['enrolment_table_role_shortname_field'] = 'Role shortname field';
$string['enrolment_table_role_shortname_field_desc'] = 'Name of the role shortname field in the enrolment table.';

$string['settingsheadercourse'] = 'Course table';
$string['course_table'] = 'Course table name';
$string['course_table_desc'] = 'Name of the course table in the SiteDirectory.';
$string['course_table_course_id_field'] = 'Course id field';
$string['course_table_course_id_field_desc'] = 'Name of the course id (id in Moodle\'s course table) field in the course table.';
$string['course_table_course_idnumber_field'] = 'Course idnumber field';
$string['course_table_course_idnumber_field_desc'] = 'Name of the course idnumber field in the course table.';
$string['course_table_course_fullname_field'] = 'Course fullname field';
$string['course_table_course_fullname_field_desc'] = 'Name of the course fullname field in the course table.';
$string['course_table_course_description_field'] = 'Course description field';
$string['course_table_course_description_field_desc'] = 'Name of the course description field in the course table.';
$string['course_table_category_idnumber_field'] = 'Category idnumber field';
$string['course_table_category_idnumber_field_desc'] = 'Name of the category idnumber field in the course table.';
$string['course_table_course_status_field'] = 'Course status field';
$string['course_table_course_status_field_desc'] = 'Name of the course status field in the course table.';
$string['course_table_course_type_field'] = 'Course type field';
$string['course_table_course_type_field_desc'] = 'Name of the course type field in the course table.';
$string['course_table_source_field'] = 'Source field';
$string['course_table_source_field_desc'] = 'Name of the source field (name of the system from which the course was synchronized) in the course table.';
$string['course_table_course_starttime_field'] = 'Course starttime field';
$string['course_table_course_starttime_field_desc'] = 'Name of the course starttime field in the course table.';
$string['course_table_course_endtime_field'] = 'Course endtime field';
$string['course_table_course_endtime_field_desc'] = 'Name of the course endtime field in the course table.';
$string['course_table_ron_stp_sp_nr_field'] = 'STP_SP_NR field';
$string['course_table_ron_stp_sp_nr_field_desc'] = 'Name of the STP_SP_NR field (the ID used in the course\'s URL in RWTHonline) in the course table.';

$string['access_sitedirectory_missing_params'] = 'Sitedirectory not usable because of missing parameters';
$string['access_sitedirectory_external_db_connection_problem'] = 'Problem connecting to the SiteDirectory';
$string['access_sitedirectory_external_db_read_error'] = 'Error reading data from SiteDirectory';
$string['sitedirectory_external_db_missing_user'] = 'User with {$a->field} \'{$a->value}\' missing from SiteDirectory';
$string['sitedirectory_external_db_invalid_courseid'] = 'Course id {$a} is invalid';
$string['no_free_couponcode'] = 'Could not find an unused coupon code to give to the new user {$a}.';
$string['couponcode_not_unique'] = 'The combination of coupon code {$a->couponcode} and course id {$a->courseid} is not unique.';

$string['settingsheaderronrole'] = 'RWTHonline role table';
$string['ronrole_table'] = 'RWTHonline Role table';
$string['ronrole_table_desc'] = 'Name of the RWTHonline Role table in the SiteDirectory.';
$string['ronrole_table_moodleid_field'] = 'Moodle-ID field';
$string['ronrole_table_moodleid_field_desc'] = 'Name of the Moodle-ID field in the RWTHonline Role table.';
$string['ronrole_table_course_idnumber_field'] = 'Course idnumber field';
$string['ronrole_table_course_idnumber_field_desc'] = 'Name of the course idnumber field in the RWTHonline Role table.';
$string['ronrole_table_role_shortname_field'] = 'Role shortname field';
$string['ronrole_table_role_shortname_field_desc'] = 'Name of the role shortname field in the RWTHonline Role table.';

$string['settingsheadercourseevent'] = 'Course event table';
$string['course_event_table'] = 'Course event table';
$string['course_event_table_desc'] = 'Name of the course event table in the SiteDirectory.';
$string['course_event_table_course_idnumber_field'] = 'Course idnumber field';
$string['course_event_table_course_idnumber_field_desc'] = 'Name of the Moodle-ID field in the course event table.';
$string['course_event_table_id_field'] = 'Notes / ID in RWTH Online';
$string['course_event_table_id_field_desc'] = 'Notes of the ID field in the course event table.';
$string['course_event_table_name_field'] = 'Name field';
$string['course_event_table_name_field_desc'] = 'Keyword of the name field in the course event table.';
$string['course_event_table_id_field'] = 'ID field';
$string['course_event_table_id_field_desc'] = 'Name of the id field in the course event table.';
$string['course_event_table_starttime_field'] = 'Start time field';
$string['course_event_table_starttime_field_desc'] = 'The start time field in the course event table.';
$string['course_event_table_endtime_field'] = 'End time field';
$string['course_event_table_endtime_field_desc'] = 'The end time in the course event table.';
$string['course_event_table_room_field'] = 'Room field';
$string['course_event_table_room_field_desc'] = 'Name of the room field in the course event table.';
$string['course_event_table_seriesno_field'] = 'Course SeriesNo field';
$string['course_event_table_seriesno_field_desc'] = 'SeriesNo of the course event table in the SiteDirectory.';

$string['settingsheadergroup'] = 'Group table';
$string['group_table'] = 'Group table';
$string['group_table_desc'] = 'Name of the the group table in the SiteDirectory.';
$string['group_table_course_idnumber_field'] = 'Course idnummer field';
$string['group_table_course_idnumber_field_desc'] = 'Name of the course idnummer field in the group table.';
$string['group_table_id_field'] = 'id field';
$string['group_table_id_field_desc'] = 'Name of the id field in the group table.';
$string['group_table_name_field'] = 'Name field';
$string['group_table_name_field_desc'] = 'Name field in the group table.';

$string['settingsheadergroupevent'] = 'Group event table';
$string['group_event_table'] = 'Group event table';
$string['group_event_table_desc'] = 'Name of the group event table in the SiteDirectory.';
$string['group_event_table_groupid_field'] = 'Group id field';
$string['group_event_table_groupid_field_desc'] = 'Name of the group id field in the group event table.';
$string['group_event_table_name_field'] = 'Name field';
$string['group_event_table_name_field_desc'] = 'Name field in the group event table.';
$string['group_event_table_id_field'] = 'ID field';
$string['group_event_table_id_field_desc'] = 'Name of the ID field in the group event table.';
$string['group_event_table_starttime_field'] = 'Start time field';
$string['group_event_table_starttime_field_desc'] = 'The start time field in the group event table.';
$string['group_event_table_endtime_field'] = 'End time field';
$string['group_event_table_endtime_field_desc'] = 'The end time field the in the group event table.';
$string['group_event_table_room_field'] = 'Room field';
$string['group_event_table_room_field_desc'] = 'Name of the room field in the group event table.';
$string['group_event_table_seriesno_field'] = 'Group SeriesNo field';
$string['group_event_table_seriesno_field_desc'] = 'SeriesNo of the group event table in the SiteDirectory.';

$string['settingsheaderevent'] = 'Event table';
$string['event_table'] = 'Event Table';
$string['event_table_desc'] = 'Event Table';
$string['event_table_kontextid_field'] = 'ID of Group / Course';
$string['event_table_kontextid_field_desc'] = 'Name of the KontextId in the event table.';
$string['event_table_id_field'] = 'Notes field';
$string['event_table_id_field_desc'] = 'Name of the Notes field in the event table.';
$string['event_table_kontextart_field'] = 'KontextArt field';
$string['event_table_kontextart_field_desc'] = 'Name of the KontextArt field in the event table.';
$string['event_table_start_field'] = 'Start field';
$string['event_table_start_field_desc'] = 'Name of the Start field in the event table.';
$string['event_table_end_field'] = 'End field';
$string['event_table_end_field_desc'] = 'Name of the End field in the event table.';
$string['event_table_room_field'] = 'Room field';
$string['event_table_room_field_desc'] = 'Room field in the event table.';
$string['event_table_seriesno_field'] = 'Seriesno field';
$string['event_table_seriesno_field_desc'] = 'Seriesno field in the event table.';
$string['event_table_keyword_field'] = 'Keyword field';
$string['event_table_keyword_field_desc'] = 'Keyword field in the event table.';
$string['event_table_kontext_field'] = 'Kontext field';
$string['event_table_kontext_field_desc'] = 'Kontext field in the event table.';

$string['settingsheaderdivecoupon'] = 'Dive Coupon table';
$string['coupon_table'] = 'Dive Coupon Table';
$string['coupon_table_desc'] = 'Dive Coupon Table';
$string['coupon_table_coupon_id_field'] = 'Coupon ID';
$string['coupon_table_coupon_id_field_desc'] = 'Coupon ID.';
$string['coupon_table_coupon_code_field'] = 'Coupon Code';
$string['coupon_table_coupon_code_field_desc'] = '"Wegwerfgeheimnis" zur Authentifizierung des Nutzenden.';
$string['coupon_table_externaltype_field'] = 'External Type';
$string['coupon_table_externaltype_field_desc'] = 'Initial Courseroom ID';
$string['coupon_table_externalid_field'] = 'External ID';
$string['coupon_table_externalid_field_desc'] = 'ID der Einladung';
$string['coupon_table_created_field'] = 'Created';
$string['coupon_table_created_field_desc'] = 'Created';
$string['coupon_table_modified_field'] = 'Updated';
$string['coupon_table_modified_field_desc'] = 'Updated';
$string['coupon_table_moodlecourseid_field'] = 'Moodle Course ID';
$string['coupon_table_moodlecourseid_field_desc'] = 'Moodle Course ID';
$string['coupon_table_rolename_field'] = 'Role name';
$string['coupon_table_rolename_field_desc'] = 'Name of the role the user will be invited as.';
$string['coupon_table_roleid_field'] = 'Role ID';
$string['coupon_table_roleid_field_desc'] = 'ID of the role the user will be invited as.';
$string['coupon_table_status_field'] = 'Status';
$string['coupon_table_status_field_desc'] = 'Status of the invitation.';
$string['coupon_table_createdby_field'] = 'Created by';
$string['coupon_table_createdby_field_desc'] = 'User who created the invitation';
$string['coupon_table_modifiedby_field'] = 'Last modified by';
$string['coupon_table_modifiedby_field_desc'] = 'User who last modified the invitation';
$string['invitation_duration'] = 'Invitation duration';
$string['invitation_duration_help'] = 'How long sent invitations are open.';
