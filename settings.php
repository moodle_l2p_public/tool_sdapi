<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 *
 * @package     tool_sdapi
 * @category    admin
 * @copyright   2019 Tim Schroeder <t.schroeder@itc.rwth-aachen.de>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {

    $settings = new admin_settingpage('tool_sdapi', new lang_string('pluginname', 'tool_sdapi'));

    $enrolsettingsurl = "$CFG->wwwroot/$CFG->admin/settings.php?section=enrolsettingsdatabase";
    $settings->add(new admin_setting_heading('tool_sdapi_settings', '', get_string('pluginname_desc', 'tool_sdapi', $enrolsettingsurl)));

    $settings->add(new admin_setting_heading(
        'tool_sdapi_userheader',
        get_string('settingsheaderuser', 'tool_sdapi'),
        ''));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/user_table',
        get_string('user_table', 'tool_sdapi'),
        get_string('user_table_desc', 'tool_sdapi'),
        'Dive_IdMapping'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/user_table_moodleid_field',
        get_string('user_table_moodleid_field', 'tool_sdapi'),
        get_string('user_table_moodleid_field_desc', 'tool_sdapi'),
        'MoodleId'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/user_table_lmsid_field',
        get_string('user_table_lmsid_field', 'tool_sdapi'),
        get_string('user_table_lmsid_field_desc', 'tool_sdapi'),
        'LmsId'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/user_table_timid_field',
        get_string('user_table_timid_field', 'tool_sdapi'),
        get_string('user_table_timid_field_desc', 'tool_sdapi'),
        'TimId'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/user_table_matrnr_field',
        get_string('user_table_matrnr_field', 'tool_sdapi'),
        get_string('user_table_matrnr_field_desc', 'tool_sdapi'),
        'MatrNr'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/user_table_gguid_field',
        get_string('user_table_gguid_field', 'tool_sdapi'),
        get_string('user_table_gguid_field_desc', 'tool_sdapi'),
        'Gguid'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/user_table_ronid_field',
        get_string('user_table_ronid_field', 'tool_sdapi'),
        get_string('user_table_ronid_field_desc', 'tool_sdapi'),
        'RonId'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/user_table_deleted_field',
        get_string('user_table_deleted_field', 'tool_sdapi'),
        get_string('user_table_deleted_field_desc', 'tool_sdapi'),
        'Deleted'));

    $settings->add(new admin_setting_heading(
        'tool_sdapi_enrolmentheader',
        get_string('settingsheaderenrolment', 'tool_sdapi'),
        ''));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/enrolment_table',
        get_string('enrolment_table', 'tool_sdapi'),
        get_string('enrolment_table_desc', 'tool_sdapi'),
        'TeilnehmerInKurs2'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/enrolment_table_moodleid_field',
        get_string('enrolment_table_moodleid_field', 'tool_sdapi'),
        get_string('enrolment_table_moodleid_field_desc', 'tool_sdapi'),
        'moodleid'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/enrolment_table_course_idnumber_field',
        get_string('enrolment_table_course_idnumber_field', 'tool_sdapi'),
        get_string('enrolment_table_course_idnumber_field_desc', 'tool_sdapi'),
        'Veranstaltung'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/enrolment_table_role_shortname_field',
        get_string('enrolment_table_role_shortname_field', 'tool_sdapi'),
        get_string('enrolment_table_role_shortname_field_desc', 'tool_sdapi'),
        'Rolle'));

    $settings->add(new admin_setting_heading(
        'tool_sdapi_courseheader',
        get_string('settingsheadercourse', 'tool_sdapi'),
        ''));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_table',
        get_string('course_table', 'tool_sdapi'),
        get_string('course_table_desc', 'tool_sdapi'),
        'Veranstaltung'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_table_course_id_field',
        get_string('course_table_course_id_field', 'tool_sdapi'),
        get_string('course_table_course_id_field_desc', 'tool_sdapi'),
        'MoodleInterneId'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_table_course_idnumber_field',
        get_string('course_table_course_idnumber_field', 'tool_sdapi'),
        get_string('course_table_course_idnumber_field_desc', 'tool_sdapi'),
        'ID'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_table_course_fullname_field',
        get_string('course_table_course_fullname_field', 'tool_sdapi'),
        get_string('course_table_course_fullname_field_desc', 'tool_sdapi'),
        'Title'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_table_course_description_field',
        get_string('course_table_course_description_field', 'tool_sdapi'),
        get_string('course_table_course_description_field_desc', 'tool_sdapi'),
        'Description'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_table_category_idnumber_field',
        get_string('course_table_category_idnumber_field', 'tool_sdapi'),
        get_string('course_table_category_idnumber_field_desc', 'tool_sdapi'),
        'Semester'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_table_course_status_field',
        get_string('course_table_course_status_field', 'tool_sdapi'),
        get_string('course_table_course_status_field_desc', 'tool_sdapi'),
        'Status'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_table_course_type_field',
        get_string('course_table_course_type_field', 'tool_sdapi'),
        get_string('course_table_course_type_field_desc', 'tool_sdapi'),
        'Art'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_table_source_field',
        get_string('course_table_source_field', 'tool_sdapi'),
        get_string('course_table_source_field_desc', 'tool_sdapi'),
        'Quellsystem'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_table_course_starttime_field',
        get_string('course_table_course_starttime_field', 'tool_sdapi'),
        get_string('course_table_course_starttime_field_desc', 'tool_sdapi'),
        'Start'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_table_course_endtime_field',
        get_string('course_table_course_endtime_field', 'tool_sdapi'),
        get_string('course_table_course_endtime_field_desc', 'tool_sdapi'),
        'End'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_table_ron_stp_sp_nr_field',
        get_string('course_table_ron_stp_sp_nr_field', 'tool_sdapi'),
        get_string('course_table_ron_stp_sp_nr_field_desc', 'tool_sdapi'),
        'STP_SP_NR'));
    $settings->add(new admin_setting_heading(
        'tool_sdapi_ronrole',
        get_string('settingsheaderronrole', 'tool_sdapi'),
        ''));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/ronrole_table',
        get_string('ronrole_table', 'tool_sdapi'),
        get_string('ronrole_table_desc', 'tool_sdapi'),
        'Teilnehmer2'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/ronrole_table_moodleid_field',
        get_string('ronrole_table_moodleid_field', 'tool_sdapi'),
        get_string('ronrole_table_moodleid_field_desc', 'tool_sdapi'),
        'MoodleId'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/ronrole_table_course_idnumber_field',
        get_string('ronrole_table_course_idnumber_field', 'tool_sdapi'),
        get_string('ronrole_table_course_idnumber_field_desc', 'tool_sdapi'),
        'Veranstaltung'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/ronrole_table_role_shortname_field',
        get_string('ronrole_table_role_shortname_field', 'tool_sdapi'),
        get_string('ronrole_table_role_shortname_field_desc', 'tool_sdapi'),
        'Rolle'));
    $settings->add(new admin_setting_heading(
        'tool_sdapi_courseevent',
        get_string('settingsheadercourseevent', 'tool_sdapi'),
        ''));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_event_table',
        get_string('course_event_table', 'tool_sdapi'),
        get_string('course_event_table_desc', 'tool_sdapi'),
        'TermineKurs'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_event_table_course_idnumber_field',
        get_string('course_event_table_course_idnumber_field', 'tool_sdapi'),
        get_string('course_event_table_course_idnumber_field_desc', 'tool_sdapi'),
        'Veranstaltung'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_event_table_name_field',
        get_string('course_event_table_name_field', 'tool_sdapi'),
        get_string('course_event_table_name_field_desc', 'tool_sdapi'),
        'Keyword'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_event_table_id_field',
        get_string('course_event_table_id_field', 'tool_sdapi'),
        get_string('course_event_table_id_field_desc', 'tool_sdapi'),
        'Notes'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_event_table_id_field',
        get_string('course_event_table_id_field', 'tool_sdapi'),
        get_string('course_event_table_id_field_desc', 'tool_sdapi'),
        'Notes'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_event_table_starttime_field',
        get_string('course_event_table_starttime_field', 'tool_sdapi'),
        get_string('course_event_table_starttime_field_desc', 'tool_sdapi'),
        'Start'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_event_table_endtime_field',
        get_string('course_event_table_endtime_field', 'tool_sdapi'),
        get_string('course_event_table_endtime_field_desc', 'tool_sdapi'),
        'End'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_event_table_room_field',
        get_string('course_event_table_room_field', 'tool_sdapi'),
        get_string('course_event_table_room_field_desc', 'tool_sdapi'),
        'Room'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/course_event_table_seriesno_field',
        get_string('course_event_table_seriesno_field', 'tool_sdapi'),
        get_string('course_event_table_seriesno_field_desc', 'tool_sdapi'),
        'SeriesNo'));
    $settings->add(new admin_setting_heading(
        'group_table',
        get_string('settingsheadergroup', 'tool_sdapi'),
        ''));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/group_table',
        get_string('group_table', 'tool_sdapi'),
        get_string('group_table_desc', 'tool_sdapi'),
        'LvGruppen'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/group_table_course_idnumber_field',
        get_string('group_table_course_idnumber_field', 'tool_sdapi'),
        get_string('group_table_course_idnumber_field_desc', 'tool_sdapi'),
        'Veranstaltung'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/group_table_id_field',
        get_string('group_table_id_field', 'tool_sdapi'),
        get_string('group_table_id_field_desc', 'tool_sdapi'),
        'GRP_ID'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/group_table_name_field',
        get_string('group_table_name_field', 'tool_sdapi'),
        get_string('group_table_name_field_desc', 'tool_sdapi'),
        'GRP_Name'));
    $settings->add(new admin_setting_heading(
        'group_event_table',
        get_string('settingsheadergroupevent', 'tool_sdapi'),
        ''));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/group_event_table',
        get_string('group_event_table', 'tool_sdapi'),
        get_string('group_event_table_desc', 'tool_sdapi'),
        'TermineGruppe'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/group_event_table_groupid_field',
        get_string('group_event_table_groupid_field', 'tool_sdapi'),
        get_string('group_event_table_groupid_field_desc', 'tool_sdapi'),
        'GRP_ID'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/group_event_table_name_field',
        get_string('group_event_table_name_field', 'tool_sdapi'),
        get_string('group_event_table_name_field_desc', 'tool_sdapi'),
        'Keyword'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/group_event_table_id_field',
        get_string('group_event_table_id_field', 'tool_sdapi'),
        get_string('group_event_table_id_field_desc', 'tool_sdapi'),
        'Notes'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/group_event_table_starttime_field',
        get_string('group_event_table_starttime_field', 'tool_sdapi'),
        get_string('group_event_table_starttime_field_desc', 'tool_sdapi'),
        'Start'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/group_event_table_endtime_field',
        get_string('group_event_table_endtime_field', 'tool_sdapi'),
        get_string('group_event_table_endtime_field_desc', 'tool_sdapi'),
        'End'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/group_event_table_room_field',
        get_string('group_event_table_room_field', 'tool_sdapi'),
        get_string('group_event_table_room_field_desc', 'tool_sdapi'),
        'Room'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/group_event_table_seriesno_field',
        get_string('group_event_table_seriesno_field', 'tool_sdapi'),
        get_string('group_event_table_seriesno_field_desc', 'tool_sdapi'),
        'SeriesNo'));

    $settings->add(new admin_setting_heading(
        'event_table',
        get_string('settingsheaderevent', 'tool_sdapi'),
        ''));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/event_table',
        get_string('event_table', 'tool_sdapi'),
        get_string('event_table_desc', 'tool_sdapi'),
        'Termine'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/event_table_kontextid_field',
        get_string('event_table_kontextid_field', 'tool_sdapi'),
        get_string('event_table_kontextid_field_desc', 'tool_sdapi'),
        'KontextId'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/event_table_kontext_field',
        get_string('event_table_kontext_field', 'tool_sdapi'),
        get_string('event_table_kontext_field_desc', 'tool_sdapi'),
        'Kontext'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/event_table_keyword_field',
        get_string('event_table_keyword_field', 'tool_sdapi'),
        get_string('event_table_keyword_field_desc', 'tool_sdapi'),
        'Keyword'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/event_table_id_field',
        get_string('event_table_id_field', 'tool_sdapi'),
        get_string('event_table_id_field_desc', 'tool_sdapi'),
        'Notes'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/event_table_kontextart_field',
        get_string('event_table_kontextart_field', 'tool_sdapi'),
        get_string('event_table_kontextart_field_desc', 'tool_sdapi'),
        'KontextArt'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/event_table_start_field',
        get_string('event_table_start_field', 'tool_sdapi'),
        get_string('event_table_start_field_desc', 'tool_sdapi'),
        'Start'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/event_table_end_field',
        get_string('event_table_end_field', 'tool_sdapi'),
        get_string('event_table_end_field_desc', 'tool_sdapi'),
        'End'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/event_table_room_field',
        get_string('event_table_room_field', 'tool_sdapi'),
        get_string('event_table_room_field_desc', 'tool_sdapi'),
        'Room'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/event_table_seriesno_field',
        get_string('event_table_seriesno_field', 'tool_sdapi'),
        get_string('event_table_seriesno_field_desc', 'tool_sdapi'),
        'SeriesNo'));

    $settings->add(new admin_setting_heading(
        'dive_coupon_table',
        get_string('settingsheaderdivecoupon', 'tool_sdapi'),
        ''));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/coupon_table',
        get_string('coupon_table', 'tool_sdapi'),
        get_string('coupon_table_desc', 'tool_sdapi'),
        'Dive_Coupon'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/coupon_table_coupon_id_field',
        get_string('coupon_table_coupon_id_field', 'tool_sdapi'),
        get_string('coupon_table_coupon_id_field_desc', 'tool_sdapi'),
        'ID'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/coupon_table_coupon_code_field',
        get_string('coupon_table_coupon_code_field', 'tool_sdapi'),
        get_string('coupon_table_coupon_code_field_desc', 'tool_sdapi'),
        'CouponCode'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/coupon_table_externaltype_field',
        get_string('coupon_table_externaltype_field', 'tool_sdapi'),
        get_string('coupon_table_externaltype_field_desc', 'tool_sdapi'),
        'ExternalType'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/coupon_table_externalid_field',
        get_string('coupon_table_externalid_field', 'tool_sdapi'),
        get_string('coupon_table_externalid_field_desc', 'tool_sdapi'),
        'ExternalId'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/coupon_table_moodlecourseid_field',
        get_string('coupon_table_moodlecourseid_field', 'tool_sdapi'),
        get_string('coupon_table_moodlecourseid_field_desc', 'tool_sdapi'),
        'MoodleKursId'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/coupon_table_rolename_field',
        get_string('coupon_table_rolename_field', 'tool_sdapi'),
        get_string('coupon_table_rolename_field_desc', 'tool_sdapi'),
        'Rolle'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/coupon_table_roleid_field',
        get_string('coupon_table_roleid_field', 'tool_sdapi'),
        get_string('coupon_table_roleid_field_desc', 'tool_sdapi'),
        'RollenId'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/coupon_table_status_field',
        get_string('coupon_table_status_field', 'tool_sdapi'),
        get_string('coupon_table_status_field_desc', 'tool_sdapi'),
        'Status'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/coupon_table_created_field',
        get_string('coupon_table_created_field', 'tool_sdapi'),
        get_string('coupon_table_created_field_desc', 'tool_sdapi'),
        'Created'));
    $settings->add(new admin_setting_configtext(
        'tool_sdapi/coupon_table_modified_field',
        get_string('coupon_table_modified_field', 'tool_sdapi'),
        get_string('coupon_table_modified_field_desc', 'tool_sdapi'),
        'Updated'));
    $settings->add(new admin_setting_configduration(
        'tool_sdapi/invitation_duration',
        get_string('invitation_duration', 'tool_sdapi'),
        get_string('invitation_duration_help', 'tool_sdapi'),
        14 * DAYSECS));

    $settings->add(new admin_setting_configtext(
        'tool_sdapi/coupon_table_createdby_field',
        get_string('coupon_table_createdby_field', 'tool_sdapi'),
        get_string('coupon_table_createdby_field_desc', 'tool_sdapi'),
        'UserIdCreatedBy'));

    $settings->add(new admin_setting_configtext(
        'tool_sdapi/coupon_table_modifiedby_field',
        get_string('coupon_table_modifiedby_field', 'tool_sdapi'),
        get_string('coupon_table_modifiedby_field_desc', 'tool_sdapi'),
        'UserIdLastUpdate'));

    $ADMIN->add('tools', $settings);
}
