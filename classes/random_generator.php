<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_sdapi;

defined('MOODLE_INTERNAL') || die;

class random_generator {

    public static function generate_couponcode($segmentlength, $numsegments, $segmentseparator,
            $prefix = '', $allowedcharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') {
        $segments = [];
        $pos = 0;
        for ($i = 0; $i < $numsegments; $i++) {
            $segment = '';
            for ($j = 0; $j < $segmentlength; $j++) {
                if ($pos < strlen($prefix)) {
                    $segment .= $prefix[$pos];
                } else {
                    $index = rand(0, strlen($allowedcharacters) - 1);
                    $segment .= $allowedcharacters[$index];
                }
                $pos++;
            }
            $segments[] = $segment;
        }
        return join($segmentseparator, $segments);
    }
}
