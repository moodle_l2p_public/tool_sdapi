<?php
// This file is part of Moodle - http://moodle.org/
// 
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_sdapi;

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/filelib.php');
require_once($CFG->dirroot.'/enrol/database/lib.php');

use stdClass;

use core_php_time_limit;
use enrol_database_plugin;
use moodle_exception;

class sitedirectory_api extends enrol_database_plugin {

    protected $extdb = null;

    const COURSE_STATUS_OPEN = 0;
    const COURSE_STATUS_CLOSED = 1;

    const SOURCE_CAMPUS = 0;
    const SOURCE_RWTHONLINE = 1;
    const SOURCE_OTHER = 1000;

    const INVITATION_STATUS_OPEN = 'open';
    const INVITATION_STATUS_REVOKED = 'revoked';
    const INVITATION_STATUS_EXPIRED = 'expired';
    const INVITATION_STATUS_COMPLETED = 'completed';

    /**
     * Constructor.
     * @throws access_sitedirectory_exception when plugin is not configured correctly
     *                                        or connection to SiteDirectory fails
     */
    public function __construct() {
        // We may need a lot of memory here.
        core_php_time_limit::raise();
        raise_memory_limit(MEMORY_HUGE);

        if (!$this->extdb = $this->db_init()) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_connection_problem');
        }
    }

    /**
     * Destructor.
     */
    public function __destruct() {
        if (!is_null($this->extdb)) {
            $this->extdb->Close();
            $this->extdb = null;
        }
    }

    /**
     * Returns a user from the SiteDirectory.
     * @param int $moodleid the user's id in Moodle
     * @return object user object
     * @throws access_sitedirectory_exception when failing to read from SiteDirectory
     * @throws user_not_found_exception when user doesn't exist in SiteDirectory
     */
    public function get_user($moodleid) {
        $field = get_config('tool_sdapi', 'user_table_moodleid_field');
        return $this->get_user_internal($field, $moodleid);
    }

     /**
     * Returns a user from the SiteDirectory.
     * @param int $timid the user's tim-id
     * @return object user object
     * @throws access_sitedirectory_exception when failing to read from SiteDirectory
     * @throws user_not_found_exception when user doesn't exist in SiteDirectory
     */
    public function get_user_by_timid($timid) {
        $field = get_config('tool_sdapi', 'user_table_timid_field');
        return $this->get_user_internal($field, $timid);
    }

    /**
     * Returns a user from the SiteDirectory.
     * @param string $field name of column to look at in user table
     * @param mixed $value value to look for in user
     * @return object user object
     * @throws access_sitedirectory_exception when failing to read from SiteDirectory
     * @throws user_not_found_exception when user doesn't exist in SiteDirectory
     */
    protected function get_user_internal($field, $value) {
        $table = get_config('tool_sdapi', 'user_table');
        $deletedfield = get_config('tool_sdapi', 'user_table_deleted_field');

        $conditions = [];
        $conditions[$field] = $value;
        $conditions[$deletedfield] = '0';

        $sql = $this->db_get_sql($table, $conditions, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }
        if ($rs->EOF) {
            $rs->Close();
            $a = (object)['field' => $field, 'value' => $value];
            throw new user_not_found_exception('sitedirectory_external_db_missing_user', '', $a);
        }
        $fields = $rs->FetchRow();
        $rs->Close();

        $fields = $this->db_decode($fields);

        $user = new stdClass();
        $user->moodleid = $fields[get_config('tool_sdapi', 'user_table_moodleid_field')];
        $user->lmsid = $fields[get_config('tool_sdapi', 'user_table_lmsid_field')];
        $user->timid = $fields[get_config('tool_sdapi', 'user_table_timid_field')];
        $user->matrnr = $fields[get_config('tool_sdapi', 'user_table_matrnr_field')];
        $user->gguid = $fields[get_config('tool_sdapi', 'user_table_gguid_field')];
        $user->ronid = $fields[get_config('tool_sdapi', 'user_table_ronid_field')];
        return $user;
    }

    /**
     * Returns all users matching the search term in any field.
     * @param string $search search term
     * @return array users matching search term
     * @throws access_sitedirectory_exception when failing to read from SiteDirectory
     */
    public function search_users($search) {
        $table = get_config('tool_sdapi', 'user_table');

        $conditionsor = []; // At least one of the following fields must contain $search.
        $conditionsor[get_config('tool_sdapi', 'user_table_lmsid_field')] = $search;
        $conditionsor[get_config('tool_sdapi', 'user_table_timid_field')] = $search;
        $conditionsor[get_config('tool_sdapi', 'user_table_matrnr_field')] = $search;
        $conditionsor[get_config('tool_sdapi', 'user_table_ronid_field')] = $search;
        $conditionsand = []; // AND deleted must be 0.
        $conditionsand[get_config('tool_sdapi', 'user_table_deleted_field')] = '0';

        $sql = $this->db_get_sql_search($table, $conditionsand, $conditionsor, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $results = [];
        while ($fields = $rs->FetchRow()) {
            $fields = $this->db_decode($fields);

            $user = new stdClass();
            $user->moodleid = $fields[get_config('tool_sdapi', 'user_table_moodleid_field')];
            $user->lmsid = $fields[get_config('tool_sdapi', 'user_table_lmsid_field')];
            $user->timid = $fields[get_config('tool_sdapi', 'user_table_timid_field')];
            $user->matrnr = $fields[get_config('tool_sdapi', 'user_table_matrnr_field')];
            $user->gguid = $fields[get_config('tool_sdapi', 'user_table_gguid_field')];
            $user->ronid = $fields[get_config('tool_sdapi', 'user_table_ronid_field')];
            $results[] = $user;
        }
        $rs->Close();
        return $results;
    }

    /**
     * Returns all enrolments for the given user.
     * @param int $moodleid the user's id in Moodle
     * @return array the user's enrolments
     * @throws access_sitedirectory_exception when failing to read from SiteDirectory
     */
    public function get_enrolments($moodleid) {
        if (!$moodleid) {
            return [];
        }
        $table = get_config('tool_sdapi', 'enrolment_table');
        $moodleidfield = get_config('tool_sdapi', 'enrolment_table_moodleid_field');

        $conditions = [];
        $conditions[$moodleidfield] = $moodleid;

        $sql = $this->db_get_sql($table, $conditions, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }
        $results = [];
        while ($fields = $rs->FetchRow()) {
            $fields = $this->db_decode($fields);

            $enrolment = new stdClass();
            $enrolment->user = $fields[$moodleidfield];
            $enrolment->course = $fields[get_config('tool_sdapi', 'enrolment_table_course_idnumber_field')];
            $enrolment->role = $fields[get_config('tool_sdapi', 'enrolment_table_role_shortname_field')];
            $results[] = $enrolment;
        }
        $rs->Close();
        return $results;
    }

    /**
     * Returns details for a course.
     * @param string $idnumber the idnumber of a course
     * @return object details of the course
     * @throws access_sitedirectory_exception when failing to read from SiteDirectory
     * @throws course_not_found_exception when course doesn't exist in SiteDirectory
     */
    public function get_course($idnumber) {
        $table = get_config('tool_sdapi', 'course_table');
        $courseidnumberfield = get_config('tool_sdapi', 'course_table_course_idnumber_field');

        $conditions = [];
        $conditions[$courseidnumberfield] = $idnumber;

        $sql = $this->db_get_sql($table, $conditions, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }
        if ($rs->EOF) {
            $rs->Close();
            throw new course_not_found_exception('sitedirectory_external_db_missing_course', '', $idnumber);
        }
        $fields = $rs->FetchRow();
        $rs->Close();

        $course = new stdClass();
        $course->id = $fields[get_config('tool_sdapi', 'course_table_course_id_field')];
        $course->idnumber = $fields[$courseidnumberfield];
        $course->fullname = $fields[get_config('tool_sdapi', 'course_table_course_fullname_field')];
        $course->description = $fields[get_config('tool_sdapi', 'course_table_course_description_field')];
        $course->category = $fields[get_config('tool_sdapi', 'course_table_category_idnumber_field')];

        $status = $fields[get_config('tool_sdapi', 'course_table_course_status_field')];
        if ($status === 'offen') {
            $course->status = self::COURSE_STATUS_OPEN;
        } else {
            $course->status = self::COURSE_STATUS_CLOSED;
        }

        $course->type = $fields[get_config('tool_sdapi', 'course_table_course_type_field')];

        $source = $fields[get_config('tool_sdapi', 'course_table_source_field')];
        if ($source === 'campus') {
            $course->source = self::SOURCE_CAMPUS;
        } else if ($source === 'RWTHonline') {
            $course->source = self::SOURCE_RWTHONLINE;
        } else {
            $course->source = self::SOURCE_OTHER;
        }

        $starttime = $fields[get_config('tool_sdapi', 'course_table_course_starttime_field')];
        if (is_numeric($starttime)) {
            $course->starttime = (int) $starttime;
        } else {
            $course->starttime = $starttime;
        }

        $endtime = $fields[get_config('tool_sdapi', 'course_table_course_endtime_field')];
        if (is_numeric($endtime)) {
            $course->endtime = (int) $endtime;
        } else {
            $course->endtime = $endtime;
        }

        $course->ron_stp_sp_nr = $fields[get_config('tool_sdapi', 'course_table_ron_stp_sp_nr_field')];
        return $course;
    }

    /**
     * Returns name of this enrol plugin
     * @return string
     */
    public function get_name() {
        return 'database';
    }

    /**
     * Same as $this->db_get_sql except that conditions are ORed instead of ANDed.
     */
    protected function db_get_sql_search($table, array $conditionsand, array $conditionsor, array $fields, $distinct = false, $sort = "") {
        $fields = $fields ? implode(',', $fields) : "*";
        $whereand = array();
        if ($conditionsand) {
            foreach ($conditionsand as $key => $value) {
                $value = $this->db_encode($this->db_addslashes($value));
                $whereand[] = "$key = '$value'";
            }
        }
        $whereor = array();
        if ($conditionsor) {
            foreach ($conditionsor as $key => $value) {
                $value = $this->db_encode($this->db_addslashes($value));
                $whereor[] = "$key = '$value'";
            }
        }

        // E.g. "WHERE conditionsand[0] AND conditionsand[1] AND (conditionsor[0] OR conditionsor[1])".
        $where = "";
        if (count($conditionsand) > 0) {
            $where .= "WHERE ".implode(" AND ", $whereand);
        }
        if (count($conditionsor) > 0) {
            $where .=  (count($conditionsand) > 0) ? " AND " : " WHERE ";
            $where .= "(" . implode(" OR ", $whereor) . ")";
         }
        $sort = $sort ? "ORDER BY $sort" : "";
        $distinct = $distinct ? "DISTINCT" : "";
        $sql = "SELECT $distinct $fields
                  FROM $table
                 $where
                  $sort";

        return $sql;
    }

    /**
     * Returns users who have the given ronrole and the given courseidnumber.
     * @param int $courseidnumber the event id (Veranstaltung id) in SD
     * @param string $ronrole the role of users in Moodle
     * @throws access_sitedirectory_exception when failing to read from SiteDirectory
     * @return moodleid from moodle users
     */
    public function get_users_with_ronrole($courseidnumber, $ronrole) {
        if (!$courseidnumber || !$ronrole) {
            return;
        }
        $table = get_config('tool_sdapi', 'ronrole_table'); // Teilnehmer2.
        $course = get_config('tool_sdapi', 'ronrole_table_course_idnumber_field'); // Veranstaltung.
        $role = get_config('tool_sdapi', 'ronrole_table_role_shortname_field'); // Rolle.

        $conditions = [];
        $conditions[$course] = $courseidnumber;
        $conditions[$role] = $ronrole;

        $sql = $this->db_get_sql($table, $conditions, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $results = [];
        while ($fields = $rs->FetchRow()) {
            $fields = $this->db_decode($fields);

            $user = new stdClass();
            $user->moodleid = $fields[get_config('tool_sdapi', 'ronrole_table_moodleid_field')]; //gets Userid.
            $results[] = $user;
        }
        $rs->Close();
        return $results;
    }

    /**
     * Returns courses from table Teilnehmer2 where user with $userid has the $ronrole.
     * @param int $userid Moodle-User with $userid 
     * @param string $ronrole the role of users in Moodle
     * @throws access_sitedirectory_exception when failing to read from SiteDirectory
     * @return lecture from moodle users
     */
    public function get_courses_where_user_has_role($userid, $ronrole) {
        if (!$userid || !$ronrole) {
            return;
        }

        $table = get_config('tool_sdapi', 'ronrole_table'); // Teilnehmer2.
        $user = get_config('tool_sdapi', 'ronrole_table_moodleid_field');
        $role = get_config('tool_sdapi', 'ronrole_table_role_shortname_field');

        $conditions = [];
        $conditions[$user] = $userid;
        $conditions[$role] = $ronrole;

        $sql = $this->db_get_sql($table, $conditions, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $results = [];
        while ($fields = $rs->FetchRow()) {
            $fields = $this->db_decode($fields);

            $course = new stdClass();
            $course->idnumber = $fields[get_config('tool_sdapi', 'ronrole_table_course_idnumber_field')];
            $results[] = $course;
        }
        $rs->Close();
        return $results;
    }

    /**
     * Returns all groups from LvGruppen Table which belongs to the course with id $courseudnumber.
     * @param int $courseidnumber Moodle-Course
     * @throws access_sitedirectory_exception when failing to read from SiteDirectory
     * @return lecture from moodle users
     */
    public function get_course_groups($courseidnumber) {
        if (!$courseidnumber) {
            return;
        }

        $table = get_config('tool_sdapi', 'group_table');
        $cid = get_config('tool_sdapi', 'group_table_course_idnumber_field');

        $condition = [];
        $condition[$cid] = $courseidnumber;

        $sql = $this->db_get_sql($table, $condition, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $results = [];
        while ($fields = $rs->FetchRow()) {
            $fields = $this->db_decode($fields);

            $temp = new stdClass();

            $temp->idnumber = $fields[get_config('tool_sdapi', 'course_event_table_course_idnumber_field')];
            $temp->groupid = $fields[get_config('tool_sdapi', 'group_table_id_field')];
            $temp->groupname  = $fields[get_config('tool_sdapi', 'group_table_name_field')];
            $results[] = $temp;
        }
        $rs->Close();
        return $results;
    }

    public function get_group_event($groupeventid) {
        if (!$groupeventid) {
            return;
        }

        $table = get_config('tool_sdapi', 'group_event_table'); //TermineGruppe.
        $cid = get_config('tool_sdapi', 'group_event_table_id_field');

        $condition = [];
        $condition[$cid] = $groupeventid;

        $sql = $this->db_get_sql($table, $condition, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $fields = $rs->FetchRow();
        $fields = $this->db_decode($fields);

        $gevent = new stdClass();
        $gevent->id = $fields[get_config('tool_sdapi', 'group_event_table_id_field')]; // Notes-row
        $gevent->starttime  = $fields[get_config('tool_sdapi', 'group_event_table_starttime_field')];
        $gevent->endtime = $fields[get_config('tool_sdapi', 'group_event_table_endtime_field')];
        $gevent->room = $fields[get_config('tool_sdapi', 'group_event_table_room_field')];
        $gevent->seriesno = $fields[get_config('tool_sdapi', 'group_event_table_seriesno_field')];

        $rs->Close();
        return $gevent;
    }

    /**
     * Gets event data from the table Termine
     * @param $eventid is ID from the Notes-Row
     */
    public function get_event_data_with_eventid($eventid) {
        if (!$eventid) {
            return;
        }

        $table = get_config('tool_sdapi', 'event_table'); //Termine.
        $gid = get_config('tool_sdapi', 'event_table_id_field');

        $condition = [];
        $condition[$gid] = $eventid;

        $sql = $this->db_get_sql($table, $condition, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $fields = $rs->FetchRow();
        $fields = $this->db_decode($fields);

        $data = new \stdClass();
        if ($fields) {
            $data->id = $fields[get_config('tool_sdapi', 'event_table_id_field')];
            $data->keyword = $fields[get_config('tool_sdapi', 'event_table_keyword_field')];
            $data->kontextart = $fields[get_config('tool_sdapi', 'event_table_kontextart_field')];
            $data->kontextid = $fields[get_config('tool_sdapi', 'event_table_kontextid_field')];
            $data->start = $fields[get_config('tool_sdapi', 'event_table_start_field')];
            $data->end = $fields[get_config('tool_sdapi', 'event_table_end_field')];
            $data->room = $fields[get_config('tool_sdapi', 'event_table_room_field')];
        }

        return $data;
    }

    public function get_courseid_with_kontextart_group($eventid) {
        if (!$eventid) {
            return;
        }

        $table = get_config('tool_sdapi', 'group_table'); //LvGruppen.
        $gid = get_config('tool_sdapi', 'group_table_id_field'); // GRP_ID.

        $condition = [];
        $condition[$gid] = $eventid;

        $sql = $this->db_get_sql($table, $condition, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $fields = $rs->FetchRow();
        $fields = $this->db_decode($fields);

        $idnumber = $fields[get_config('tool_sdapi', 'group_table_course_idnumber_field')];

        return $idnumber;
    }

    public function get_groupname($groupid) {
        if (!$groupid) {
            return;
        }

        $table = get_config('tool_sdapi', 'group_table'); //TermineGruppe.
        $gid = get_config('tool_sdapi', 'group_table_id_field');

        $condition = [];
        $condition[$gid] = $groupid;

        $sql = $this->db_get_sql($table, $condition, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $fields = $rs->FetchRow();
        $fields = $this->db_decode($fields);

        $idnumber = $fields[get_config('tool_sdapi', 'group_table_name_field')];

        return $idnumber;
    }

    public function get_courseid_with_kontextart_course($eventid) {
        if (!$eventid) {
            return;
        }

        $table = get_config('tool_sdapi', 'course_event_table');
        $cid = get_config('tool_sdapi', 'course_event_table_id_field');

        $condition = [];
        $condition[$cid] = $eventid;

        $sql = $this->db_get_sql($table, $condition, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $fields = $rs->FetchRow();
        $fields = $this->db_decode($fields);

        $idnumber = $fields[get_config('tool_sdapi', 'course_event_table_course_idnumber_field')];

        $rs->Close();
        return $idnumber;
    }

    public function get_eventids_with_seriesno($seriesno, $contextart) {
        if (!$seriesno) {
            return;
        }

        $table = get_config('tool_sdapi', 'event_table'); //Termine.
        $sno = get_config('tool_sdapi', 'event_table_seriesno_field');
        $art = get_config('tool_sdapi', 'event_table_kontextart_field');

        $conditions = [];
        $conditions[$sno] = $seriesno;
        $conditions[$art] = $contextart;

        $sql = $this->db_get_sql($table, $conditions, [], false, "Start ASC");

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $results = [];
        while ($fields = $rs->FetchRow()) {
            $fields = $this->db_decode($fields);

            $event = new stdClass();
            $event->eventid = $fields[get_config('tool_sdapi', 'event_table_id_field')];
            $results[] = $event;
        }

        $rs->Close();
        return $results;
    }

    public function get_group_seriesno_with_groupid($groupid) {
        if (!$groupid) {
            return;
        }

        $table = get_config('tool_sdapi', 'group_event_table'); //TermineGruppe.
        $cid = get_config('tool_sdapi', 'group_event_table_groupid_field');

        $condition = [];
        $condition[$cid] = $groupid;

        $sql = $this->db_get_sql($table, $condition, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $results = [];
        while ($fields = $rs->FetchRow()) {
            $fields = $this->db_decode($fields);

            $gevent = new stdClass();
            $gevent->groupid = $fields[get_config('tool_sdapi', 'group_event_table_groupid_field')];
            $gevent->eventid = $fields[get_config('tool_sdapi', 'group_event_table_id_field')];
            $gevent->seriesno = $fields[get_config('tool_sdapi', 'group_event_table_seriesno_field')];
            $results[] = $gevent;
        }

        $rs->Close();
        return $results;
    }

    public function get_course_seriesno_with_courseidnumber($courseidnumber) {
        if (!$courseidnumber) {
            return;
        }

        $table = get_config('tool_sdapi', 'course_event_table'); //TermineKurs.
        $cid = get_config('tool_sdapi', 'course_event_table_course_idnumber_field');

        $condition = [];
        $condition[$cid] = $courseidnumber;

        $sql = $this->db_get_sql($table, $condition, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $results = [];
        while ($fields = $rs->FetchRow()) {
            $fields = $this->db_decode($fields);

            $temp = new stdClass();
            $temp->seriesno = $fields[get_config('tool_sdapi', 'course_event_table_seriesno_field')];
            $temp->eventid = $fields[get_config('tool_sdapi', 'course_event_table_id_field')];
            $results[] = $temp;
        }

        $rs->Close();
        return $results;
    }

    public function get_course_category($courseidnumber) {
        if (!$courseidnumber) {
            return;
        }

        $table = get_config('tool_sdapi', 'course_table');
        $cid = get_config('tool_sdapi', 'course_table_course_idnumber_field');

        $condition = [];
        $condition[$cid] = $courseidnumber;

        $sql = $this->db_get_sql($table, $condition, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $fields = $rs->FetchRow();
        $fields = $this->db_decode($fields);

        $cat = ($fields) ? $fields[get_config('tool_sdapi', 'course_table_category_idnumber_field')] : false;

        $rs->Close();
        return $cat;
    }

    /**
     * @param $eventid, $kontextart
     * @return $id groupid / courseid
     */
    public function get_id_with_eventid($eventid, $kontextart) {
        if (!$eventid) {
            return;
        }

        $table = get_config('tool_sdapi', 'event_table'); //Termine.
        $eid = get_config('tool_sdapi', 'event_table_id_field');
        $art = get_config('tool_sdapi', 'event_table_kontextart_field');

        $conditions = [];
        $conditions[$eid] = $eventid;
        $conditions[$art] = $kontextart;

        $sql = $this->db_get_sql($table, $conditions, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $fields = $rs->FetchRow();
        $fields = $this->db_decode($fields);

        $id = $fields[get_config('tool_sdapi', 'event_table_kontext_field')];

        return $id;
    }

    /**
     * @param $cid courseid
     */
    public function get_course_idnumber($cid) {
        if (!$cid) {
            return;
        }

        $table = get_config('tool_sdapi', 'course_table'); //Termine.
        $courseid = get_config('tool_sdapi', 'course_table_course_id_field');

        $conditions = [];
        $conditions[$courseid] = $cid;

        $sql = $this->db_get_sql($table, $conditions, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $fields = $rs->FetchRow();
        $fields = $this->db_decode($fields);

        $idnumber = $fields[get_config('tool_sdapi', 'course_table_course_idnumber_field')];

        return $idnumber;
    }

    public function get_group_ids($cidnumber) {
        if (!$cidnumber) {
            return;
        }

        $table = get_config('tool_sdapi', 'group_table'); //LvGruppe.
        $cid = get_config('tool_sdapi', 'group_table_course_idnumber_field');

        $conditions = [];
        $conditions[$cid] = $cidnumber;

        $sql = $this->db_get_sql($table, $conditions, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $results = [];
        while ($fields = $rs->FetchRow()) {
            $fields = $this->db_decode($fields);

            $gevent = new stdClass();
            $gevent->groupid = $fields[get_config('tool_sdapi', 'group_table_id_field')];
            $results[] = $gevent;
        }

        $rs->Close();
        return $results;
    }

    public function get_invitaions_for_course($courseid) {
        if (!$courseid || $courseid == SITEID) {
            return [];
        }

        $table = get_config('tool_sdapi', 'coupon_table');
        $moodlecourseidfield = get_config('tool_sdapi', 'coupon_table_moodlecourseid_field');
        if (!$table || !$moodlecourseidfield) {
            // Most likely an upgrade is in progress and default values for new settings
            // are not set yet. This can happen when upgrading local_rwth_participants_view
            // and tool_sdapi at the same time.
            return [];
        }

        $conditions = [];
        $conditions[$moodlecourseidfield] = $courseid;

        $sql = $this->db_get_sql($table, $conditions, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $results = [];
        $openduration = get_config('tool_sdapi', 'invitation_duration');
        while ($fields = $rs->FetchRow()) {
            $fields = $this->db_decode($fields);

            $coupon = new stdClass();
            $coupon->id = $fields[get_config('tool_sdapi', 'coupon_table_coupon_id_field')];
            $coupon->couponcode = $fields[get_config('tool_sdapi', 'coupon_table_coupon_code_field')];
            $coupon->externaltype = $fields[get_config('tool_sdapi', 'coupon_table_externaltype_field')];
            $coupon->externalid = $fields[get_config('tool_sdapi', 'coupon_table_externalid_field')];
            $coupon->moodlecourseid = $fields[get_config('tool_sdapi', 'coupon_table_moodlecourseid_field')];
            $coupon->roleid = $fields[get_config('tool_sdapi', 'coupon_table_roleid_field')];
            $coupon->rolename = format_string($fields[get_config('tool_sdapi', 'coupon_table_rolename_field')]);
            $coupon->status = $fields[get_config('tool_sdapi', 'coupon_table_status_field')];
            $coupon->created = strtotime($fields[get_config('tool_sdapi', 'coupon_table_created_field')]);
            $coupon->modified = strtotime($fields[get_config('tool_sdapi', 'coupon_table_modified_field')]);
            if ($coupon->status == self::INVITATION_STATUS_OPEN) {
                $coupon->expires = $coupon->modified + $openduration;
            }
            $coupon->createdby = $fields[get_config('tool_sdapi', 'coupon_table_createdby_field')];
            $results[] = $coupon;
        }

        $rs->Close();
        return $results;
    }

    public function get_invitations_for_email($email, $courseid) {
        if (empty($email) || !$courseid || $courseid == SITEID) {
            return [];
        }

        $table = get_config('tool_sdapi', 'coupon_table');
        $moodlecourseidfield = get_config('tool_sdapi', 'coupon_table_moodlecourseid_field');
        $externalidfield = get_config('tool_sdapi', 'coupon_table_externalid_field');

        $conditions = [];
        $conditions[$externalidfield] = $email;
        $conditions[$moodlecourseidfield] = $courseid;

        $sql = $this->db_get_sql($table, $conditions, []);

        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $results = [];
        $openduration = get_config('tool_sdapi', 'invitation_duration');
        while ($fields = $rs->FetchRow()) {
            $fields = $this->db_decode($fields);

            $coupon = new stdClass();
            $coupon->id = $fields[get_config('tool_sdapi', 'coupon_table_coupon_id_field')];
            $coupon->couponcode = $fields[get_config('tool_sdapi', 'coupon_table_coupon_code_field')];
            $coupon->externaltype = $fields[get_config('tool_sdapi', 'coupon_table_externaltype_field')];
            $coupon->externalid = $fields[get_config('tool_sdapi', 'coupon_table_externalid_field')];
            $coupon->moodlecourseid = $fields[get_config('tool_sdapi', 'coupon_table_moodlecourseid_field')];
            $coupon->rolename = format_string($fields[get_config('tool_sdapi', 'coupon_table_rolename_field')]);
            $coupon->status = $fields[get_config('tool_sdapi', 'coupon_table_status_field')];
            $coupon->created = strtotime($fields[get_config('tool_sdapi', 'coupon_table_created_field')]);
            $coupon->modified = strtotime($fields[get_config('tool_sdapi', 'coupon_table_modified_field')]);
            if ($coupon->status == self::INVITATION_STATUS_OPEN) {
                $coupon->expires = $coupon->modified + $openduration;
            }
            $results[] = $coupon;
        }

        $rs->Close();
        return $results;
    }

    public function get_invitation($couponcode, $courseid) {
        if (!$courseid || !$couponcode) {
            return;
        }

        $table = get_config('tool_sdapi', 'coupon_table');
        $couponidfield       = get_config('tool_sdapi', 'coupon_table_coupon_id_field');
        $couponcodefield     = get_config('tool_sdapi', 'coupon_table_coupon_code_field');
        $externaltypefield   = get_config('tool_sdapi', 'coupon_table_externaltype_field');
        $externalidfield     = get_config('tool_sdapi', 'coupon_table_externalid_field');
        $moodlecourseidfield = get_config('tool_sdapi', 'coupon_table_moodlecourseid_field');
        $roleshortnamefield  = get_config('tool_sdapi', 'coupon_table_rolename_field');
        $roleidfield         = get_config('tool_sdapi', 'coupon_table_roleid_field');
        $statusfield         = get_config('tool_sdapi', 'coupon_table_status_field');
        $createdfield        = get_config('tool_sdapi', 'coupon_table_created_field');
        $modifiedfield       = get_config('tool_sdapi', 'coupon_table_modified_field');

        $conditions = [];
        $conditions[$couponcodefield] = $couponcode;
        $conditions[$moodlecourseidfield] = $courseid;

        $sql = $this->db_get_sql($table, $conditions, []);
        $rs = $this->extdb->Execute($sql);
        if (!$rs) {
            throw new access_sitedirectory_exception('access_sitedirectory_external_db_read_error');
        }

        $invitations = [];
        $openduration = get_config('tool_sdapi', 'invitation_duration');
        while ($fields = $rs->FetchRow()) {
            $fields = $this->db_decode($fields);

            $coupon = new stdClass();
            $coupon->id = $fields[$couponidfield];
            $coupon->couponcode = $fields[$couponcodefield];
            $coupon->externaltype = $fields[$externaltypefield];
            $coupon->externalid = $fields[$externalidfield];
            $coupon->moodlecourseid = $fields[$moodlecourseidfield];
            $coupon->rolename = format_string($fields[$roleshortnamefield]);
            $coupon->roleid = $fields[$roleidfield];
            $coupon->status = $fields[$statusfield];
            $coupon->created = strtotime($fields[$createdfield]);
            $coupon->modified = strtotime($fields[$modifiedfield]);
            if ($coupon->status == self::INVITATION_STATUS_OPEN) {
                $coupon->expires = $coupon->modified + $openduration;
            }
            $invitations[] = $coupon;
        }

        $rs->Close();
        if (empty($invitations)) {
            return false;
        } else if (count($invitations) > 1) {
            $a = new stdClass();
            $a->couponcode = $couponcode;
            $a->courseid = $courseid;
            throw new moodle_exception('couponcode_not_unique', 'tool_sdapi', '', $a);
        }
        return array_values($invitations)[0];
    }

    /**
     * @return string generated coupon code
     */
    public function add_invitation($email, $courseid, $roleid, $rolename) {
        global $USER;
        if (!$courseid || $courseid == SITEID) {
            throw new access_sitedirectory_exception('sitedirectory_external_db_invalid_courseid', '', $courseid);
        }

        $table = get_config('tool_sdapi', 'coupon_table');
        $couponcodefield     = get_config('tool_sdapi', 'coupon_table_coupon_code_field');
        $statusfield         = get_config('tool_sdapi', 'coupon_table_status_field');
        $externaltypefield   = get_config('tool_sdapi', 'coupon_table_externaltype_field');
        $externalidfield     = get_config('tool_sdapi', 'coupon_table_externalid_field');
        $moodlecourseidfield = get_config('tool_sdapi', 'coupon_table_moodlecourseid_field');
        $rolenamefield       = get_config('tool_sdapi', 'coupon_table_rolename_field');
        $roleidfield         = get_config('tool_sdapi', 'coupon_table_roleid_field');
        $createdfield        = get_config('tool_sdapi', 'coupon_table_created_field');
        $modifiedfield       = get_config('tool_sdapi', 'coupon_table_modified_field');
        $createdbyfield      = get_config('tool_sdapi', 'coupon_table_createdby_field');
        $modifiedbyfield     = get_config('tool_sdapi', 'coupon_table_modifiedby_field');

        // Try to generate an unused coupon code.
        $foundunusedcouponcode = false;
        for ($i = 0; $i < 50; $i++) {
            $couponcode = random_generator::generate_couponcode(5, 4, '-', '2Y');
            $sql = $this->db_get_sql($table, [
                $couponcodefield => $couponcode,
                $statusfield => self::INVITATION_STATUS_OPEN
            ], []);
            $rs = $this->extdb->Execute($sql);
            if (!$rs->FetchRow()) {
                $foundunusedcouponcode = true;
                $rs->Close();
                break;
            }
            $rs->Close();
        }
        if (!$foundunusedcouponcode) {
            throw new no_free_couponcode_exception('no_free_couponcode', '', $email);
        }
        $timestamp = $this->extdb->DBTimeStamp(time());

        $fields = [];
        $fields[$couponcodefield]     = $couponcode;
        $fields[$statusfield]         = self::INVITATION_STATUS_OPEN;
        $fields[$externaltypefield]   = 'InitialCourseroomId';
        $fields[$externalidfield]     = $email;
        $fields[$moodlecourseidfield] = $courseid;
        $fields[$rolenamefield]       = $rolename;
        $fields[$roleidfield]         = $roleid;
        $fields[$createdfield]        = $timestamp;
        $fields[$modifiedfield]       = $timestamp;
        $fields[$createdbyfield]      = $USER->id;
        $fields[$modifiedbyfield]     = $USER->id;

        $sql = $this->extdb->GetInsertSQL($table, $fields);
        $this->extdb->Execute($sql);

        return $couponcode;
    }

    public function update_invitation_status($invitationid, $status) {
        global $USER;
        if (!$invitationid || !in_array($status, [
            self::INVITATION_STATUS_OPEN,
            self::INVITATION_STATUS_COMPLETED,
            self::INVITATION_STATUS_EXPIRED,
            self::INVITATION_STATUS_REVOKED,
        ])) {
            return;
        }

        $table = get_config('tool_sdapi', 'coupon_table');
        $extstatus = get_config('tool_sdapi', 'coupon_table_status_field');
        $extid = get_config('tool_sdapi', 'coupon_table_coupon_id_field');
        $extmodified = get_config('tool_sdapi', 'coupon_table_modified_field');
        $extusermodified = get_config('tool_sdapi', 'coupon_table_modifiedby_field');

        $modified = $this->extdb->DBTimeStamp(time());
        $userid = $USER->id;

        $sql = "UPDATE $table
                SET $extstatus = ?,
                    $extmodified = $modified,
                    $extusermodified = $userid
                WHERE $extid = ?";
        $params = [
            $status,
            $invitationid,
        ];
        $this->extdb->Execute($sql, $params);
    }

    public function expire_invitations() {
        $table = get_config('tool_sdapi', 'coupon_table');
        $statusfield = get_config('tool_sdapi', 'coupon_table_status_field');
        $modifiedfield = get_config('tool_sdapi', 'coupon_table_modified_field');

        $openduration = get_config('tool_sdapi', 'invitation_duration');
        $cutoffdate = $this->extdb->DBTimeStamp(usergetmidnight(time() - $openduration));
        $open = self::INVITATION_STATUS_OPEN;
        $expired = self::INVITATION_STATUS_EXPIRED;

        $sql = "UPDATE $table
                SET $statusfield = '$expired'
                WHERE $statusfield = '$open'
                    AND $modifiedfield < $cutoffdate";
        $this->extdb->Execute($sql);
    }
}
