<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace tool_sdapi\task;

use tool_sdapi\sitedirectory_api;

defined('MOODLE_INTERNAL') || die();

/**
 * Sets status of invitations to "expired" if they have been open for too long.
 *
 * @package    tool_sdapi
 * @copyright  2022 RWTH Aachen
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class expire_invitations extends \core\task\scheduled_task {

    public function get_name() {
        return get_string('expire_invitations_task', 'tool_sdapi');
    }

    public function execute() {
        $api = new sitedirectory_api();
        $api->expire_invitations();
    }
}
